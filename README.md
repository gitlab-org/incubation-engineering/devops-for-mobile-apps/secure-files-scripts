# Secure Files Scripts

## Overview

This project contains a few helper scripts that can be used to interact with WIP Secure Files API in GitLab. Please note, this feature is still in development and has not been enabled on gitlab.com. You can follow along with development here: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/75695

## Usage

Below is a sample .gitlab-ci.yml file that can be used to test Secure Files in a CI pipeline. This example expects wget and ruby are installed on the runner instance.

```
stages:
  - test

test:
  script: 
    - wget https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/secure-files-scripts/-/raw/main/secure_file_loader.rb
    - ruby secure_file_loader.rb
    - ls -lah .secure_files
```