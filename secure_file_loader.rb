require 'digest'
require 'fileutils'
require 'json'
require "open-uri"

# specify the location where all Secure Files should be downloaded within the runner job, defaults to `.secure_files`
$download_path = '.secure_files'

# configure the script from the CI environment variables
$api_v4_url = ENV['CI_API_V4_URL']
$project_id = ENV['CI_PROJECT_ID']
$job_token  = ENV['CI_JOB_TOKEN']
$base_uri = "#{$api_v4_url}/projects/#{$project_id}/secure_files"

# fetches and downloads all the Secure Files for a given project
def fetch_secure_files
  begin 
    URI.open($base_uri, "JOB-TOKEN" => $job_token){|res|
      files = JSON.parse(res.read)

      if files.count.zero?
        puts "No Secure Files found in project"
      else
        create_download_path
        puts "Secure File data loaded, downloading files to #{$download_path}"

        files.each do |file|
          download_file(file)
        end
      end
    }
  rescue OpenURI::HTTPError => msg
    puts "Unable to load Secure Files - #{msg}"
  end
end

# creates the Secure Files destination folder if it does not exist
def create_download_path
  FileUtils.mkdir_p($download_path) unless File.directory?($download_path)
end

# downloads an individual Secure File to the download path and validates the checksum
# will raise an exception and fail the job if the checksum validation fails
def download_file(file)
  file_id  = file['id']
  filename = file['name']
  checksum = file['checksum']

  uri = URI("#{$base_uri}/#{file_id}/download")

  begin
    destination_file = "#{$download_path}/#{filename}"
    # TODO: update file write to support file permissions returned from the API
    File.open(destination_file, "wb") do |saved_file|
      URI.open(uri, "rb", "JOB-TOKEN" => $job_token) do |data|
        saved_file.write(data.read)
      end
    end

    raise "Checksum validation failed for #{destination_file}" unless checksum_correct?(destination_file, checksum)

    puts "#{destination_file} saved"

  rescue OpenURI::HTTPError => msg
    puts "Unable to download #{filename} - #{msg}"
  end
end

# validates the checksum for a downloaded file
def checksum_correct?(file, checksum)
  Digest::SHA256.hexdigest(File.read(file)) == checksum
end

fetch_secure_files